//
//  ViewController.swift
//  dev-profile
//
//  Created by Oforkanji Odekpe on 1/10/18.
//  Copyright © 2018 Oforkanji Odekpe. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var devslopesImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        devslopesImage.layer.cornerRadius = self.devslopesImage.frame.width / 5
        devslopesImage.layer.masksToBounds = true
    }

}

